/**
 * 冒泡排序函数
 * aa bb cc
 * @param a 待排序的数组
 * @param n 待排序的数组长度
 */
public static void bubbleSort(int [] a, int n){
    int changeFlag;
    for (int i = 0; i < n - 1; i++) {
        changeFlag = 0;
        for (int j = 0; j < n - i - 1; ++j) {
            if (a[j] > a[j + 1]) {
                a[j] ^= a[j + 1];
                a[j + 1] ^= a[j];
                a[j] ^= a[j + 1];
                changeFlag = 1;
            }
        }
        if (changeFlag == 0) {
            break;
        }
    }
} //end

