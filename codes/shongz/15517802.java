/**  
 * 冒泡排序函数  
 * 该函数实现了冒泡排序算法，将数组中的元素按照从小到大的顺序排列  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int [] a, int n){  
    for (int i = 0; i < n - 1; i++) { // 外层循环，表示遍历所有未排序的元素  
        for (int j = 0; j < n - 1 - i; j++) { // 内层循环，用于比较相邻的元素并进行交换  
            if (a[j] > a[j + 1]) { // 如果前一个元素大于后一个元素，则交换它们的位置  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
} //end
