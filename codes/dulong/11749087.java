/**
 * 冒泡排序函数
 * aa bb cc
 * @param a 待排序的数组
 * @param n 待排序的数组长度
 */
public static void bubbleSort(int[] a, int n) {
    if (n <= 1) return;
    for (int i = 0; i < n; ++i) {
        boolean flag = false;
        for (int j = 0; j < n - i - 1; ++j) {
            if (a[j] > a[j+1]) { 
                int tmp = a[j];
                a[j] = a[j+1];
                a[j+1] = tmp;
                flag = true;  
            }
        }
        if (!flag) break;
    }
} //end
